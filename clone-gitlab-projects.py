#!/usr/bin/env python3

import gitlab
# import json
import os
import subprocess
import sys
import getpass
import argparse


GITLAB_URL = 'https://www.gitlab.com'
GIT_DIR = '~/git/gitlab/'


class Gitlaber():
    """ Gitlab explorer """

    def __init__(self, token):
        """ Initialize variables """
        self.gl = gitlab.Gitlab(GITLAB_URL, token, api_version=4)  # per_page=)

    def list_groups(self):
        """ List groups within your Organization """

        groups = list()

        for gr in self.gl.groups.list(all=True, as_list=False,
                                      obey_rate_limit=False):
            group = dict()

            if 'squads' in gr.attributes['full_path']:
                continue

            group['id'] = gr.attributes['id']
            group['full_path'] = gr.attributes['full_path']

            groups.append(group)

        return groups

    def get_projects(self, group_id):
        """Get all projects under selected group"""

        group = self.gl.groups.get(group_id)
        projects = group.projects.list(all=True)
        if projects:
            for project in projects:
                # Clone project
                try:
                    os.chdir(os.path.expanduser(GIT_DIR))
                    if not os.path.isdir(project.path_with_namespace):
                        os.makedirs(project.path_with_namespace)
                        print(f'\033[1mCreating: '
                              f'{project.path_with_namespace}\033[0m')
                    os.chdir(project.path_with_namespace)
                except Exception as exc:
                    print(exc)
                    sys.exit(1)

                try:
                    if any(os.scandir(os.getcwd())):
                        print(f'\033[1mPulling {project.name}\033[0m')
                        subprocess.run(
                            ['git', 'pull', '--ff-only'],
                            check=True
                        )
                    else:
                        subprocess.run(
                         ['git', 'clone', f'{project.ssh_url_to_repo}', '.'],
                         check=True
                        )

                except subprocess.CalledProcessError:
                    print('The command returned an error')
                    sys.exit(1)
                except KeyboardInterrupt:
                    print('Interrupted. Aborting..')
                    sys.exit(1)

        else:
            print('Group selected does not have any projects')


def main(args):

    # Do we have the TOKEN in the environment variables?
    TOKEN = os.environ.get('GITLAB_TOKEN')

    # Do we want to override it with an argument?
    if args.token:
        TOKEN = args.token

    # Ask for a token if it was not present in the arguments or via environment
    if TOKEN is None:
        TOKEN = getpass.getpass('Enter TOKEN: ')

    if not TOKEN:
        print('You must enter a valid TOKEN')
        sys.exit(1)

    # Initialize Gitlaber class
    gl = Gitlaber(TOKEN)

    if args.group:
        gl.get_projects(args.group)
    else:
        groups = gl.list_groups()

        # Check if execution is from a local group path
        # Extract group path from current path
        pwd = os.getcwd()
        git_dir = os.path.expanduser(GIT_DIR)
        local_group = dict()
        local_group['full_path'] = pwd.replace(git_dir, '')

        # Get ID if it matches with an existing group
        for gr in groups:
            if local_group['full_path'] == gr['full_path']:
                local_group['id'] = gr['id']

        # Support more than one execution
        not_done = bool
        while not_done:

            # Ask for a group if it is not possible to obtain it from the path
            if not local_group.get('id', ''):

                # Show all groups/subgroups
                for gr in sorted(groups, key=lambda k: k['full_path']):
                    if not os.path.isdir(os.path.expanduser(GIT_DIR)
                                         + gr['full_path']
                                         ):
                        print(f'\033[31m{gr["full_path"]}\033[0m:')
                    else:
                        print(f'\033[32m{gr["full_path"]}\033[0m:')

                # Ask for a valid group
                while not local_group.get('id', ''):
                    answer = input('\nPlease enter group full path: ')
                    try:
                        full_path = str(answer)
                        for gr in groups:
                            if full_path == gr['full_path']:
                                local_group['full_path'] = gr['full_path']
                                local_group['id'] = gr['id']
                        if not local_group.get('id', ''):
                            print('Group not found.'
                                  'Please enter a valid group path')
                        else:
                            break
                    except ValueError:
                        print("That's not a valid path!")

            # Get projects from group
            print('Getting all the projects from '
                  f'\033[1m{local_group["full_path"]}\033[0m:')
            gl.get_projects(local_group['id'])

            answer = str
            while answer not in ['y', 'n']:
                answer = input('\nDo you want to clone projects'
                               ' from another group/subgroup?: (Y/N) '
                               ).lower()
            if answer == 'n':
                print('We are done. Exiting..')
                sys.exit(0)
            else:
                local_group = dict()


if __name__ == '__main__':
    # Parse command arguments
    parser = argparse.ArgumentParser(description='Clone Gitlab projects')
    parser.add_argument('-g', '--group', type=str,
                        help='Group ID')
    parser.add_argument('-t', '--token', type=str,
                        help='Gitlab TOKEN')

    args = parser.parse_args()

    main(args)
